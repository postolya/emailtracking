(function() {
    'use strict';

    angular.module('singApp', [
        'singApp.core',
        'singApp.dashboard',
        'singApp.another',
        'singApp.login',
        'singApp.error',
        'singApp.registration'
    ])
    .run(function($rootScope, $state, $stateParams, authorizationFactory, principalFactory){
		$rootScope.$on('$stateChangeStart', 
	        function(event, toState, toStateParams){
	        $rootScope.toState = toState;
	        $rootScope.toStateParams = toStateParams;
	        if (principalFactory.isIdentityResolved()) 
	            authorizationFactory.authorize();
      	});
	})
})();
