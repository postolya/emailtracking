(function() {
  'use strict';

  var module = angular.module('singApp.dashboard', [
    'ui.router',
    'singApp.components.nvd3',
    'singApp.components.sparkline',
    'singApp.coreReporting'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.dashboard', {
        url: '/dashboard',
        templateUrl: 'app/modules/dashboard/dashboard.html',
        controller: 'DashboardController'
      })
  }
})();
