(function() {
  'use strict';

    dashboardController.$inject = ['$scope', 'd3', 'nv' ,'coreReportingService','$http'];
    function dashboardController ($scope, d3, nv, coreReportingService, $http) {
    	jQuery('#datetimepicker1').datetimepicker();
    	jQuery('#datetimepicker2').datetimepicker();
    	    $scope.applyNvd3Data = function(){
      /* Inspired by Lee Byron's test data generator. */
      function _stream_layers(n, m, o) {
	        if (arguments.length < 3) o = 0;
	        function bump(a) {
	          var x = 1 / (.1 + Math.random()),
	            y = 2 * Math.random() - .5,
	            z = 10 / (.1 + Math.random());
	          for (var i = 0; i < m; i++) {
	            var w = (i / m - y) * z;
	            a[i] += x * Math.exp(-w * w);
	          }
	        }
	        return d3.range(n).map(function() {
	          var a = [], i;
	          for (i = 0; i < m; i++) a[i] = o + o * Math.random();
	          for (i = 0; i < 5; i++) bump(a);
	          return a.map(function(d, i) {
	            return {x: i, y: Math.max(0, d)};
	          });
	        });
	      }

	      function testData(stream_names, pointsCount) {
	        var now = new Date().getTime(),
	          day = 1000 * 60 * 60 * 24, //milliseconds
	          daysAgoCount = 60,
	          daysAgo = daysAgoCount * day,
	          daysAgoDate = now - daysAgo,
	          pointsCount = pointsCount || 45, //less for better performance
	          daysPerPoint = daysAgoCount / pointsCount;
	        return _stream_layers(stream_names.length, pointsCount, .1).map(function(data, i) {
	          return {
	            key: stream_names[i],
	            values: data.map(function(d,j){
	              return {
	                x: daysAgoDate + d.x * day * daysPerPoint,
	                y: Math.floor(d.y * 100) //just a coefficient,
	              }
	            })
	          };
	        });
	      }

	      $scope.nvd31Chart = nv.models.lineChart()
	        .useInteractiveGuideline(true)
	        .margin({left: 28, bottom: 30, right: 0})
	        .color(['#82DFD6', '#ddd']);

	      $scope.nvd31Chart.xAxis
	        .showMaxMin(false)
	        .tickFormat(function(d) { return d3.time.format('%b %d')(new Date(d)) });

	      $scope.nvd31Chart.yAxis
	        .showMaxMin(false)
	        .tickFormat(d3.format(',f'));

	      $scope.nvd31Data = testData(['Search', 'Referral'], 50).map(function(el, i){
	        el.area = true;
	        return el;
	      });

	      $scope.nvd32Chart = nv.models.multiBarChart()
	        .margin({left: 28, bottom: 30, right: 0})
	        .color(['#F7653F', '#ddd']);

	      $scope.nvd32Chart.xAxis
	        .showMaxMin(false)
	        .tickFormat(function(d) { return d3.time.format('%b %d')(new Date(d)) });

	      $scope.nvd32Chart.yAxis
	        .showMaxMin(false)
	        .tickFormat(d3.format(',f'));

	      $scope.nvd32Data = testData(['Uploads', 'Downloads'], 10).map(function(el, i){
	        el.area = true;
	        return el;
	      });
	      $scope.sendToken = function(){
	      	var email = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail();
	      	token=coreReportingService.getToken();
	      	token.email = email;
	      	$http.post('/user/secure/register/gmail',token);	      	
	      };
	    };

	    $scope.applyNvd3Data();
	    
	    $scope.sparklinePieData = [2,4,6];
	    $scope.sparklinePieOptions = {
	      type: 'pie',
	      width: '100px',
	      height: '100px',
	      sliceColors: ['#F5CB7B', '#FAEEE5', '#f0f0f0']
	    };
	    coreReportingService.init();
	    var link = window.location.href;
	    var authCode;

	    var dataRes;
	    if(!link.indexOf('code=')+1){
	    	authCode = link.substring(link.indexOf('code=')+5,link.indexOf('#'))
	    	$http({
	    		method:'POST',
	    		url:'https://www.googleapis.com/oauth2/v4/token',
	    		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			    transformRequest: function(obj) {
			        var str = [];
			        for(var p in obj)
			        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			        return str.join("&");
			    },
	    		data:{
		    		code:authCode,
		    		client_id:'984178848717-gvl6k38nkqm0r4n3ffq98m2snru4qqig.apps.googleusercontent.com',
		    		client_secret:'bnkEj8m9_M6LCTWMbeo2y1SA',
		    		redirect_uri:'http://alfa-track.com',
		    		grant_type:'authorization_code'
	    		}
	    	}).then(function(res){
	    		console.log(res)
	    		dataRes = res.data;
	    	})
	    }
	    $scope.offlineToken = '';
	    $scope.sendInstance = function(){
	    	var instance = {};
	    	instance.mail = coreReportingService.getInstance().currentUser.get().getBasicProfile().getEmail();
	    	coreReportingService.getOfflinePromice()
	    	.then(function(res){
	    		instance.authorizationCode = res.code
	    		console.log(instance)
	    		$http({
	    			url:'/user/secure/register/gmail',
	    			method:'POST',
	    			data:instance,
	    			headers:{
	    				'Content-Type':'application/json',
	    				'User-Id': 1
	    			}
	    		})
	    	})
	    }
    }

  angular.module('singApp.dashboard')
    .controller('DashboardController', dashboardController);

})();
