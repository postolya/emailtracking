(function() {
  'use strict';

  angular.module('singApp.coreReporting')
    .service('coreReportingService', coreReportingServiceFactory)
  ;

  coreReportingServiceFactory.$inject = [];
  function coreReportingServiceFactory () {
    var that = this;
    var clientID ='984178848717-gvl6k38nkqm0r4n3ffq98m2snru4qqig.apps.googleusercontent.com';
    var secret ='bnkEj8m9_M6LCTWMbeo2y1SA';
    // var clientID ='1083538791644-p5lk8023j0174ernqdmj83e8hm7gcdkc.apps.googleusercontent.com';
    // var secret ='CLAlxlhejoz5DiVWHkD2LzGK';

    var apiScopes ='https://mail.google.com/';
    var autorized = false;
    this.init = function(){
      gapi.load('client:auth2', that.authorise);
    };
    this.authorise = function(evnt){
      var that=this;
      gapi.auth2.init({
        client_id: clientID,
        scope: apiScopes
      })
    };
    this.signIn = function(){
        gapi.auth2.getAuthInstance().signIn();
    }
    // this.getAnaltycs = function (res) {
    //   gapi.client.load('analytics', 'v3').then(function(){
    //     autorized = true;
    //     that.getData();
    //   });
    // };
    this.getOfflinePromice = function(){
      console.log('kek')
      return this.getInstance().grantOfflineAccess({
        'scope': 'email',
        'redirect_uri': 'postmessage'
      })
      // .then(function(res){
      //   console.log(res,'kek')
      // })
    }
    this.sendQuery = function(query){
      var apiQuery = gapi.client.analytics.data.ga.get({});
    };
    this.getData = function(){
      gapi.client.analytics.management.accounts.list().then(function(e){
        if (e.result.items && e.result.items.length) {
          var firstAccountId = e.result.items[1].id;
          queryProperties(firstAccountId);
        }
      })
    };
    this.handleSigninClick = function (event) {
      gapi.auth2.getAuthInstance().signIn().then(function() {
        that.getAnaltycs();
      });
    };
    this.getToken = function(){
      return  gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse();
    }
    this.getInstance = function(){
      return gapi.auth2.getAuthInstance();
    }
  }
})();
