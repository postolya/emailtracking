(function() {
  'use strict';

  var module = angular.module('singApp.registration', [
    'ui.router',
    'ui.jq'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('registration', {
        url: '/registration',
        templateUrl: 'app/modules/registration/registration.html',
        controller: 'RegistrationController',
        data: {
          roles: []
        }
      })
  }
})();
