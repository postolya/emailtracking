(function() {
  'use strict';

    registrationController.$inject = ['$scope', '$http'];
    function registrationController ($scope, $http) {
    	$scope.sendRegistration = function(){
    		$http.post('http://alfa-track.com/user/open/registration',{
    			"name":$scope.username,
    			"email":$scope.mail, 
    			"password":$scope.pass, 
    			"passwordRepeated":$scope.passtest, 
    			"role":"USER"
    		}).then(function(data){
    			console.log(data)
    		})
    	}
    }

  angular.module('singApp.registration')
    .controller('RegistrationController', registrationController);

})();
