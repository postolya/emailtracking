(function() {
  'use strict';

  angular
    .module('singApp.core')
    .controller('App', AppController)
    .factory('jQuery', jQueryService)
    .factory('$exceptionHandler', exceptionHandler)
    .factory('authorizationFactory', authorizationFactory)
    .factory('principalFactory', principalFactory)
  ;

  AppController.$inject = ['config', '$scope', '$localStorage', '$state'];
  function AppController(config, $scope, $localStorage, $state) {
    /*jshint validthis: true */
    var vm = this;

    vm.title = config.appTitle;

    $scope.app = config;
    $scope.$state = $state;

    if (angular.isDefined($localStorage.state)){
      $scope.app.state = $localStorage.state;
    } else {
      $localStorage.state = $scope.app.state;
    }
  }

  jQueryService.$inject = ['$window'];

  function jQueryService($window) {
    return $window.jQuery; // assumes jQuery has already been loaded on the page
  }

  exceptionHandler.$inject = ['$log', '$window', '$injector'];
  function exceptionHandler($log, $window, $injector) {
    return function (exception, cause) {
      var errors = $window.JSON.parse($window.localStorage.getItem('sing-angular-errors')) || {};
      errors[new Date().getTime()] = arguments;
      $window.localStorage.setItem('sing-angular-errors', $window.JSON.stringify(errors));
      $injector.get('config').debug && $log.error.apply($log, arguments);
      $injector.get('config').debug && $window.alert('check errors');
    };
  }
  authorizationFactory.$inject = ['$rootScope', '$state', 'principalFactory'];

  function authorizationFactory ($rootScope, $state, principalFactory) {
    return {
      authorize: function() {
        return principalFactory.identity()
          .then(function() {
            var isAuthenticated = principalFactory.isAuthenticated();

            if ($rootScope.toState.data.roles
                && $rootScope.toState
                             .data.roles.length > 0 
                && !principalFactory.isInAnyRole(
                   $rootScope.toState.data.roles))
            {
              if (isAuthenticated) {
                  $state.go('app.dashboard');
              } else {
                $rootScope.returnToState
                    = $rootScope.toState;
                $rootScope.returnToStateParams
                    = $rootScope.toStateParams;
                $state.go('login');
              }
          }
        });
      }
    };
  }
  principalFactory.$inject = ['$q', '$http', '$timeout'];

  function principalFactory ($q, $http, $timeout) {
    var _identity = undefined,
      _authenticated = false;
    return {
      isIdentityResolved: function() {
        return angular.isDefined(_identity);
      },
      isAuthenticated: function() {
        return _authenticated;
      },
      isInRole: function(role) {
        if (!_authenticated || !_identity.role) 
          return false;
        return _identity.role.indexOf(role) != -1;
      },
      isInAnyRole: function(roles) {
        if (!_authenticated || !_identity.role) 
          return false;
        for (var i = 0; i < roles.length; i++) {
          if (this.isInRole(roles[i])) 
            return true;
        }
        return false;
      },
      authenticate: function(identity) {
        _identity = identity;
        _authenticated = identity != null;
      },
      identity: function(force) {
        var deferred = $q.defer();
        if (force === true) _identity = undefined;
        if (angular.isDefined(_identity)) {
          deferred.resolve(_identity);
          return deferred.promise;
        }
        $http.get('/user/secure/current', 
                  { ignoreErrors: true })
             .success(function(data) {
                 _identity = data;
                 _authenticated = true;
                 deferred.resolve(_identity);
             })
             .error(function () {
                 _identity = null;
                 _authenticated = false;
                 deferred.resolve(_identity);
             });
        return deferred.promise;
      }
    };
  }
})();
