(function() {
  'use strict';

    loginController.$inject = ['$scope', '$http','$state'];
    function loginController ($scope, $http, $state) {
    	$scope.login = function(){
    		$http({
                method:'POST',
                url:'/user/login',
                headers:{'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    console.log(str)
                    return str.join("&");
                },
                data:{
        			"username":$scope.username,
        			"password":$scope.pass
        		}
            }).then(function(data){
                $state.go('app.dashboard')
    		})
    	}
    }

  angular.module('singApp.login')
    .controller('LoginController', loginController);

})();
