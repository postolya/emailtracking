function alfaTrack(trackerId, dimensions, mailpatters){
	var ga = window[window['GoogleAnalyticsObject'] || 'ga'];
	function providePlugin(pluginName, pluginConstructor) {
	  if (typeof ga == 'function') {

	    ga('provide', pluginName, pluginConstructor);
	    ga('require', 'mailGrabber');

	   }
	};
	var mailGrabber = function(tracker, config){
		this.tracker = tracker;
		if (tracker.get('trackingId') === trackerId) {
	        this.run(tracker,dimensions,mailpatters);
	    }
	};
	mailGrabber.prototype.run = function(tracker,dimensions,mailpatters){
		var cID = tracker.get('clientId');
		plugin = this;
		var subCID = cID.replace(/\D/g,'').slice(cID.length-6,cID.length);
		this.swapMail(document.body,subCID,mailpatters)
		this.swapAdress(subCID,mailpatters);	
		var coockie = this.getCoockie('alfatrack');
		var options = {
		    'nonInteraction': 1,
		    hitCallback : function(){
		   		plugin.setCookie('alfatrack',cID,{
					path : '/',
					expires : '86400000'
				});
		    }
		};
		options[dimensions[0]] = subCID;
	    options[dimensions[1]] = cID;
		if(!coockie || coockie !== cID){
			ga('send',
				'event',
				'emailtracking',
				'updatemail',
				options
			);
		};
	}
	mailGrabber.prototype.swapAdress = function(cID,mailpatters){
		var links = document.getElementsByTagName('a');
		for (link in links) {
			for (var i = mailpatters.length - 1; i >= 0; i--) {
				var re = mailpatters[i]; 
				if(links[link].href.search(mailpatters[i])!== -1){
					var str = links[link].href
					var name   = str.substring(0, str.lastIndexOf("@"));
			        var domain = str.substring(str.lastIndexOf("@"));
			        links[link].href = name + "+"+cID+domain;
				}
			}
		}
	};
	mailGrabber.prototype.swapMail = function(node,cID,mailpatters){
			var children = node.childNodes;
		    for (var k = 0; k < children.length; k++){
		       this.swapMail(children[k],cID,mailpatters);
		    }
		    if(node.nodeType === 3){
		    	for (var i = mailpatters.length - 1; i >= 0; i--) {
		    		if(node.data.search(mailpatters[i])!== -1){
				        var name   = node.data.substring(0, node.data.lastIndexOf("@"));
				        var domain = node.data.substring(node.data.lastIndexOf("@"));
				        node.data = name + "+"+cID+domain;
		    		}
		    	}
		    }
	};
	mailGrabber.prototype.getCoockie = function(name){
		var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
		return matches ? decodeURIComponent(matches[1]) : undefined;
	};
	mailGrabber.prototype.setCookie = function(name, value, options){
		options = options || {};

		var expires = options.expires;

		if (typeof expires == "number" && expires) {
			var d = new Date();
			d.setTime(d.getTime() + expires * 1000);
			expires = options.expires = d;
		}
		if (expires && expires.toUTCString) {
			options.expires = expires.toUTCString();
		}

		value = encodeURIComponent(value);

		var updatedCookie = name + "=" + value;

		for (var propName in options) {
			updatedCookie += "; " + propName;
			var propValue = options[propName];
			if (propValue !== true) {
			  updatedCookie += "=" + propValue;
			}
		}
		document.cookie = updatedCookie;
	}
	providePlugin('mailGrabber', mailGrabber);
};
alfaTrack('UA-81071806-1',['dimension1','dimension2'],['marketing@alfa-track.com']);
